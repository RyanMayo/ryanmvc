<?php

// *********************** Database connection singleton
class Databaseinstance{
    private static $dbInstance = NULL;
    public function getDbConnection(){
        
        if (Databaseinstance::$dbInstance == NULL){
            
            try{
                Databaseinstance::$dbInstance = new PDO('mysql:host=127.0.0.1;dbname=ryanmvc', 'root', '');
                
            } catch(Exception $e) {
                die($e->getMessage());
            }
            
            return Databaseinstance::$dbInstance;
            
        } else{
            return Databaseinstance::$dbInstance;
        }
    }
}