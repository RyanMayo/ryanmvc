<?php

class PostController{
    
    public function __construct($params, $session){
        
        $this->params = $params;
        $this->session = $session;
        $this->limit = 10;     //limit of posts per page in get some functions
        $this->order = TRUE;  //sets order to TRUE = DESC FALSE = ASC
    }
    
    private function userError($postReturn, $message){ //temp user error function
        $userError[] = $message;
        var_dump(get_defined_vars());
        include ROOT_PATH. 'views/home/index.php';
        
    }
    
    public function insertpost(){

/* note had to change php.ini $_POST size and upload_max_filesize otherwise there can be a mem overflow error that makes the $_POST['data'] ""  */
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $post = new Post();
            
            $data = [];
            $data['post'] = $_POST['post'];//collects post or ''
            $file = [];
            $file = $_FILES['uploaded_file'];//collects file data or ''
            
            if($file['name'] == '' && $data['post'] == ''){//make sure at least 1 field is used
                
                $this->userError(NULL, 'Empty post, please input.');
                
            }else{
                
                if($file['name'] == '' || $file['name'] == NULL){
                    $postId = $post->insertPost($data['post']); //if file name ''/NULL sends post w/out file
                    header('Location:'. WEB_PATH .'post/getpost/id/'.$postId);  // sends user to see post
                    
                }else{  //if file has a real name 
                    $file = $post->fileMIMEValidation($file); //checks for MIME type
                    
                    if(!$post->isExtValidation($file['fileExt'])){ //checks against allowed MIME types
                        
                        $this->userError($data['post'], 'Must be jpg, bmp, png, or gif');
                        
                    }else{
                        $postId = $post->insertPost($data['post']);//sends the post & gets post id
                        $file['name'] = $post->processFileName($file, $postId); //cleans up filename and adds post id to end
                        if($post->uploadFile($file)){//uploads file & returns true or false
                            $post->updatePost($postId, 'file', $file['name']);//if uploade true post database is updated to contain filename
                            header('Location:'. WEB_PATH .'post/getpost/id/'.$postId);// sends user to see post
                        }else{//if upload fails (false)
                            $this->userError(NULL, 'File Upload Error, but the Post went through.');
                            
                        }
                    }
                }
            }
        } else {
            include ROOT_PATH. 'views/home/index.php';
        }
    }       
    
    public function getpost(){
        
        $data = [];
        $data['postID'] = $this->params['id'];
        
        $post = new Post();
        $gotPost = $post->getPost($data);
        include(ROOT_PATH. "views/home/post.php");
    }
    
    public function getallposts(){
        
        $post = new Post();
        $gotPost = $post->getAllPosts();
        include(ROOT_PATH. "views/home/post.php");
    }
    
    public function getsomeposts(){
        
        $data = [];
        $data['page'] = $this->params['page'];
        $data['limit'] = $this->limit;
        $data['order'] = $this->order;
        $data['offset'] = $this->params['page'] * $data['limit'];
        
        $post = new Post();
        $gotPost = $post->getSomePosts($data);
        include(ROOT_PATH. "views/home/post.php");
    }
    
    public function getalluserposts(){
         
        $data = [];
        $data['userID'] = $this->params['userid'];
        $data['order'] = $this->order;
        
        $post = new Post();
        $gotPost = $post->getAllUserPosts($data);
        include(ROOT_PATH. "views/home/post.php");
    }
    
    public function getsomeuserposts(){
        
        $data = [];
        $data['order'] = $this->order;
        $data['userID'] = $this->params['userid'];
        $data['page'] = $this->params['page'];
        $data['limit'] = $this->limit;
        $data['offset'] = $this->params['page'] * $this->limit;
        
        $post = new Post();
        $gotPost = $post->getSomeUserPosts($data);
        include(ROOT_PATH. "views/home/post.php");
    }
    
}