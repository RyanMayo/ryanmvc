<?php

class HomeController extends Controller{
    
    public function __construct($params, $session, $view) {
        parent::__construct($params, $session, $view);
         
    }
    
    public function index(){
        $post = new Post();
        $gotAllPosts = $post->getAllPosts();
        var_dump($gotAllPosts);
    }
    
    public function broke(){
        echo "404.";
    }
    
    public function info(){
        phpinfo();
    }
    
    public function testsessionget(){
        echo $this->session->getData('it');
        var_dump($_SESSION);
    }
}