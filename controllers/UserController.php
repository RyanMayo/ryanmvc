<?php

class UserController extends Controller{
    
    public function __construct($params, $session, $view) {
        parent::__construct($params, $session, $view);
         
    }
    
    public function index(){
        echo 'default action method';
    }

        public function profile(){
        
        $data =[];
        $data['userid'] = $this->params['id'];
        $user = new User();
        $data['user'] = $user->getUserById($data['userid']);
        $data['session'] = $this->session;
        include(ROOT_PATH .'views/user/profile.php');  //set id?
    }
    
    public function registration(){
        
        $data = [];
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $data['username'] = $_POST['username'];
            $data['password'] = $_POST['password'];
            $data['passwordverify'] = $_POST['passwordverify'];
            
            $user = new User();
            $userExists = $user->userExists($data['username']);
            
            if(!$userExists && $data['password'] == $data['passwordverify']){
                $userid = $user->registerUser($data['username'], $data['password']);
                
                if($userid){
                    $this->session->setData('userId', $userid);
                    $rememberMe = new Rememberme();
                    $token = $rememberMe->createRecord($data['username']);
         //create remember_me cookie and set the remember_me record on sql table
                    setcookie('remember_me', $data['username'].':'.$token, time()+2592000, '/');   /*259200 is a month*/ 
                    $rememberMe = new Rememberme();
                    $rememberMe->createRecord($data['username']);
                    header('Location:'. WEB_PATH .'user/profile/id/'.$userid);
                    
                }else{
                $data['failed'] = TRUE;
                include(ROOT_PATH .'views/user/registration.php');
            }
            
            }else{
                $data['failed'] = TRUE;
                include(ROOT_PATH .'views/user/registration.php');
            }
            var_dump($data);
            
        }else{
            include(ROOT_PATH."views/user/registration.php");
        }
    }
    
    public function login(){
        
        $data = [];
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $data['username'] = $_POST['username'];
            $data['password'] = $_POST['password'];
            $user = new User();
            $validLogin = $user->validateLogin($data['username'], $data['password']);
            
            if($validLogin){
                $data['user'] = $user->getUserByName($data['username']);
                $this->session->setData('userId', $data['user']['id']);
                $rememberMe = new Rememberme();
                $token = $rememberMe->createRecord($data['username']);
        //create remember_me cookie and set the remember_me record on sql table
                setcookie('remember_me', $data['username'].':'.$token, time()+2592000, '/');   /*259200 is a month*/ 
                header('Location:'. WEB_PATH .'user/profile/id/'.$data['user']['id']);  //$userid not set
                
            }else{
                $data['failed'] = TRUE;
                include(ROOT_PATH .'views/user/login.php');
            }
            //var_dump($data);
            
        }else{
            include(ROOT_PATH."views/user/login.php");
        }
    }
    
    public function logout(){
        
        $this->session->destroy();
        setcookie('remember_me', '' , time()-86400 /*negative day*/ , '/');
        $rememberMe = new Rememberme();
        
        if(isset($_COOKIE['remember_me'])){
            $rememberMe->deleteRecord($_COOKIE['remember_me']);
        }
        
        header('Location:'. WEB_PATH .'user/login');
    }
}
