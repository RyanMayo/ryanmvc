<?php
//******************************BOOTSTRAP
/*

route examples:
http://ryanmvc.local/post/getallposts
http://ryanmvc.local/post/getpost/id/260
http://ryanmvc.local/post/insertpost
http://ryanmvc.local/user/profile/id/3
http://ryanmvc.local/user/login
http://ryanmvc.local/user/logout

*/

define("ROOT_PATH",realpath(__DIR__ ). "/");
define("WEB_PATH",'http://' . $_SERVER['SERVER_NAME'] .'/');

include(ROOT_PATH. "core/Session.php");
include(ROOT_PATH. 'Databaseinstance.php');
include(ROOT_PATH. 'core/Model.php');
include(ROOT_PATH. 'core/Controller.php');
include(ROOT_PATH. 'core/View.php');

spl_autoload_register(function ($forgottenclass){
    include(ROOT_PATH .'models/'. $forgottenclass .'.php');
});

include(ROOT_PATH .'core/Router.php');

$session = new Session();

$router = new Router($_SERVER['REQUEST_URI']);
$router->loadControllerFireAction($session);
