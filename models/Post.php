<?php

class Post extends Model{
    
    public function __construct() {
        parent::__construct();
    }
    
    private function setOrder($order){

//Sets the order of returned posts either ASCending or DESCending
        if($order == TRUE){
            return 'DESC';
        }else{
            return'ASC';
            
        }
    }
    
    private function columnValidator($column){
        if($column == 'file'){
            return 'file';
        }elseif ($column == 'post'){
            return 'post';
        }elseif ($column == 'user_id'){
            return 'user_id';
        }elseif ($column == 'timestamp'){
            return 'timestamp';
        }else {
            return NULL;
        }
    }
    
    public function insertPost($postData){
        
        
        $query = $this->db->prepare('INSERT INTO posts (post) VALUES (:post)');
        $binds = array(
            ':post' => $postData
        );
        $result = $query->execute($binds);
        
        if($result){
            return $this->db->lastInsertId();
        } else {
            return FALSE;
        }
    }
    
    public function fileMIMEValidation($file){
        

        $file['name']=strtolower($file['name']);
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $file['fileExt'] = $finfo->file($file['tmp_name']);
        $i = explode('/', $file['fileExt']);
        $file['fileExt'] = '.'. $i[1];
        
        return $file;
    }
    
    public function isExtValidation($ext){
        if(
                $ext == '.gif' ||
                $ext == '.jpg' ||
                $ext == '.jpeg'||
                $ext == '.png' ||
                $ext == '.bmp' 
            ){
                return TRUE;
            }else{
                return FALSE;
            }
    }
    
    public function processFileName($file, $postID){
        
/* The file is excluded from all other data(1).  Next any non-alphanumeric characters are stripped from the filename(2).  Whitespace is replaced with '-'(3) and the file name is reduced to less than 175 charaqcters(4).  The post id fromwhich the file was generated is added to the end of the file name after a hyphen(5).  Finally the newly generated filename is set as the name of the attachment which is copied over from the temp folder(6). */
        
        $justFileName = pathinfo($file['name'],PATHINFO_FILENAME);//(1)
        $justFileName = preg_replace("/[^[:alnum:][:space:]]/ui", '-', $justFileName); //(2)
        $justFileName = preg_replace('/\s+/', '', $justFileName); //(3)
        $justFileName = substr ( $justFileName , -174); //(4)
        $newFileName = $justFileName. '-'. $postID. $file['fileExt']; //(5)
        return $newFileName;
    }
    
    public function uploadFile($file){
        
        $filePath = ROOT_PATH. 'uploaded_files/';
        
        if(is_uploaded_file($file['tmp_name'])){
            if(!copy($file['tmp_name'], $filePath.$file['name'])){ //(6)
                return FALSE;
            }else{
                return TRUE;
            }
        }
    }
    
    public function updatePost($id, $column, $value){

        $column = $this->columnValidator($column); //screens for expected values
        if($column == NULL){
            throw new Exception('Invalid Column for posts Table.');
        }
        $stmnt = 'UPDATE posts SET '. $column .' = :value WHERE id = :id';
        $query = $this->db->prepare($stmnt);

        $binds = array(
            ':id' => $id,
            ':value' => $value // change value
        );
        
        $query->execute($binds);
    }

    public function getPost($data){
        
        $postID = $data['postID'];
        
        $stmnt = 'SELECT * FROM posts WHERE id = :postID';
        $query = $this->db->prepare($stmnt);
        
        $binds = array(
            ':postID' => $postID
        );
        $query->execute($binds);
        $result[] = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getAllPosts($data = TRUE){
        
        $order = $this->setOrder($data['order']);
        
        $stmnt = 'SELECT * FROM posts ORDER BY timestamp '. $order;
        $query = $this->db->prepare($stmnt);
        
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getSomePosts($data) {
        
        $offset = $data['offset'];
        $limit = $data['limit'];
        $order = $data['order'];
        
        $stmnt = 'SELECT * FROM posts ORDER BY timestamp '. $order. ' LIMIT ?, ?';
        $query = $this->db->prepare($stmnt);
        
        $query->bindParam(1, $offset, PDO::PARAM_INT);
        $query->bindParam(2, $limit, PDO::PARAM_INT);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getAllUserPosts($data){
        
        $userID = $data['userID'];
        $order = $this->setOrder($data['order']);
        
        $stmnt = 'SELECT * FROM posts WHERE user_id = :userID ORDER BY timestamp '. $order;
        $query = $this->db->prepare($stmnt);
        
        $binds = array(
            ':userID' => $userID
        );
        $query->execute($binds);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getSomeUserPosts($data) {

        $userID = $data['userID'];
        $limit = $data['limit'];
        $offset = $data['offset'];
        $order = $this->setOrder($data['order']);

        $stmt = 'SELECT * FROM posts WHERE user_id = ? ORDER BY timestamp '. $order .' LIMIT ?, ?';
        $query = $this->db->prepare($stmt);

        $query->bindParam(1, $userID, PDO::PARAM_INT);
        $query->bindParam(2, $offset, PDO::PARAM_INT);
        $query->bindParam(3, $limit, PDO::PARAM_INT); //
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
       
}
   