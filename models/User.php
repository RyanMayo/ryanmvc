<?php

class User extends Model{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function userExists($username){
        
        $query = $this->db->prepare('SELECT * FROM users WHERE username = :username');
        $binds = array(
            ':username' => $username
        );
        $query->execute($binds);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if ($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function registerUser($username, $password){
        
        $query = $this->db->prepare('INSERT INTO users (username, password) VALUES (:username, :password)');
        $binds = array(
            ':username' => $username,
            ':password' => password_hash($password, PASSWORD_BCRYPT)
        );
        $result = $query->execute($binds);
        
        if($result){
            return $this->db->lastInsertId();
            
        } else {
            return FALSE;
        }
    }
    
    public function getUserById($id){
        
        $query = $this->db->prepare('SELECT * FROM users WHERE id = :id');
        $binds = array(
            ':id' => $id
        );
        $query->execute($binds);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getUserByName($username){
        
        $query = $this->db->prepare('SELECT * FROM users WHERE username = :username');
        $binds = array(
            ':username' => $username
        );
        $query->execute($binds);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function validateLogin($username, $password){
        
        $user = $this->getUserByName($username);
        
        if($user){
            return password_verify($password, $user['password']);
            
        }else{
            return FALSE;
        }
    }
}