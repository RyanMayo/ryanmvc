<?php

class Rememberme extends Model{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getRecord($username, $token){
        
        $query = $this->db->prepare('SELECT * FROM remember_me WHERE username = :username AND token = :token');
        $binds = array(
            ':username' => $username,
            ':token' => $token
        );
        $query->execute($binds);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function createRecord($username){
        
        $token = $this->getToken(50);
        $query = $this->db->prepare('INSERT INTO remember_me (username, token) VALUES (:username, :token)');
        $binds = array(
            ':username' => $username,
            ':token' => $token
        );
        $result = $query->execute($binds);
        
        if($result){
            return $token;
            
        } else {
            return FALSE;
        }
    }
    
    private function crypto_rand_secure($min, $max){
        
//lifted and edited from: http://solvedstack.com/questions/php-how-to-generate-a-random-unique-alphanumeric-string
        
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private function getToken($length){
        
//lifted and edited from: http://solvedstack.com/questions/php-how-to-generate-a-random-unique-alphanumeric-string
        
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    
    public function deleteRecord($rememberMeCookie){
        
        $parts = explode(':', $_COOKIE['remember_me']);
        $username = array_shift($parts);
        $token = implode(':', $parts);
        $query = $this->db->prepare('DELETE FROM remember_me WHERE username = :username AND token = :token LIMIT 1');
        $binds = array(
            ':username' => $username,
            ':token' => $token
        );
        $result = $query->execute($binds);
        return $result;
    }
}