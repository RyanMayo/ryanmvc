<?php

class View{
    
    private $templatePath;
    
    public function __construct() {
        $this->templatePath = ROOT_PATH. 'views/';
    }
    
    
    public function loadTemplate($templateName, $data = [], $contentOnly = FALSE){
        $filePath = $this->templatePath. strtolower($templateName). '.php';
        if(is_readable($filePath) && is_file($filePath)){
           
            if(!$contentOnly){
                //include my html header template <head></head> stuff
                //include my body header
                echo 'DRUPPP';
            }
            include($filePath);
            if(!$contentOnly){
                
                //generic footer here!!
            }
            return TRUE;
        }else{
            throw new Exception('Template not found, or not readable');
            return FALSE;
        }
    }
    
    public function getUrl($path = '', $params = []){
        $url = WEB_PATH;
        if($path){
            $url .= $path;
        }
        foreach($params as $key => $value){
            $url .='/'.$key.'/'.$value;
        }
        return $url;
    }
    
}

