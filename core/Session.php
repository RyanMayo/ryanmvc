<?php
//  Session is a shell for using the $_SESSION superglobal
//  
class Session{
    
    public function __construct(){
        
        session_start();
        session_regenerate_id();
        $user = new User();
        
        //first check if there is a userId in $_session
        if(isset($_SESSION['userId'])){
            
            // if there is a userId this puts the rest of the user_data
            //  into $_SESSION
           $_SESSION['user'] = $user->getUserById($_SESSION['userId']); 
        
        //if there isn't a userId in session   
        }else{
            
            //check for a $_rememberme cookie
            //is cookie: getrecord check if the cookie matches a recorded cookie
                // -and if this is the case sends back TRUE
                // -if TRUE use getUserByName on the exploeded cookie to assign 
                // -both userId from userdata[id] and user from userdata
                // -then deletes and makes new cookie
            //no cookie skips all this
            if(isset($_COOKIE['remember_me'])){
                // ryan:token# is what the cookie structure looks like
                $parts = explode(':', $_COOKIE['remember_me']);
                $username = array_shift($parts);
                $token = implode(':', $parts);
                
                $rememberMe = new Rememberme();
                $record = $rememberMe->getRecord($username, $token);
                
                if($record){
                    $userdata = $user->getUserByName($username);
                    $_SESSION['userId'] = $userdata['id'];
                    $_SESSION['user'] = $userdata;
            //delete previous remember_me token and set new token
                    $rememberMe->deleteRecord($_COOKIE['remember_me']);
                    $token = $rememberMe->createRecord($username);
                    setcookie('remember_me', $username.':'.$token, time()+2592000, '/');   /*259200 is a month*/ 
                }
            }
        }
        session_write_close();
    }
    
    public function setData($key, $value){
        session_start();
        $_SESSION[$key] = $value;
        session_write_close();
    }
    
    public function getData($key){
        session_start();
        session_write_close();
        
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
            
        }else{
            return FALSE;
        }
    }
    
    public function destroy(){
        
        session_start();
        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();
        session_write_close();
    }
}