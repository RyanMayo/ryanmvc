<?php

abstract class Controller{
    
    protected $session;
    protected $params;
    protected $view;
    
    public function __construct($params, $session, $view){
        $this->session = $session;
        $this->params = $params;
        $this->view = $view;
    }
    
    abstract public function index();
    
    protected function send404(){
        header("HTTP/1.0 404 Not Found");
        echo '<h1> 404 </h1> <p>page not found</p>';
        die();
    }
    
    public function verifyAuthenticated(){
        $user = $this->session->getData('userId');
        if(!$user){
            $userError[] = 'Please log in to post.';
            include(ROOT_PATH. 'views/user/login.php');
            die();
        }else{
            return $user;
        }
        
    }
    
    
    
}

