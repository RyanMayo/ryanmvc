<?php
//  The router maps URLs to controllers and action methods
class Router{
    
    public $controller = NULL;
    public $action = NULL;
    public $params = [];
    
    public function __construct($route){
        
        $route = explode('?',$route)[0];
        $route = strtolower($route);
        $route = trim($route, '/');
        
        if($route == ''){
            $routeBits = [];
            
        } else {
            $routeBits = explode('/',$route);
        }
        
        if(empty($routeBits)){
            $this->controller = 'home';
            $this->action = 'index';
            
        } else {
            
            $this->controller = array_shift($routeBits);
            
            if (empty($routeBits)){
                $this->action = 'index';
                
            } else {
                $this->action = array_shift($routeBits);
            }
            
            while (!empty($routeBits)){
                $key = array_shift($routeBits);
                $value = array_shift($routeBits);
                
                if (isset($key) && isset($value)){
                    $this->params[$key] = $value;
                }
            }
        }
    }
    
    public function loadControllerFireAction($session){
        
        $controllerName = ucfirst($this->controller) .'Controller';
        $controllerPath = ROOT_PATH ."controllers/". $controllerName .".php";
        
        if(is_readable($controllerPath) && is_file($controllerPath)){
            include($controllerPath);
            $controller = new $controllerName($this->params, $session, new View());
            
            
        } else {
            header("HTTP/1.0 404 Not Found");
            die('Page not found');
        }
        
        if(is_callable(array($controller, $this->action))){
            $controller->{$this->action}();
            
        } else {
            header("HTTP/1.0 404 Not Found");
            die('Page not found');
        }
    }
}
